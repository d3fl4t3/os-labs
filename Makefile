.PHONY: all clean

all: monitor signal

monitor: monitor.c
	gcc $^ -lpthread -o $@

signal: signal.c
	gcc $^ -lpthread -o $@

clean:
	rm -f monitor
	rm -f signal

#include <pthread.h>
#include <stdio.h>

typedef struct {
    pthread_cond_t cond;
    pthread_mutex_t lock;
    int ready;
    int data;
} synchronized_data;

synchronized_data channel1 = {
    .lock = PTHREAD_MUTEX_INITIALIZER, 
    .cond = PTHREAD_COND_INITIALIZER,
    .ready = 0, 
    .data = 0
};

void provide_data(synchronized_data *channel, int data) {
	while (1) {
		pthread_mutex_lock(&channel->lock);
		if (channel->ready == 0) {
			break;
		}
		pthread_mutex_unlock(&channel->lock);
	}
	channel->data = data;
	channel->ready = 1;
	pthread_cond_signal(&channel->cond);
	pthread_mutex_unlock(&channel->lock); 
}

int consume_data(synchronized_data *channel) {
    pthread_mutex_lock(&channel->lock);
    while (channel->ready == 0) {
        pthread_cond_wait(&channel->cond, &channel->lock);
    }
    int data = channel->data;
    channel->ready = 0;
    pthread_mutex_unlock(&channel->lock); 
    return data;
}

void* worker(void* a) {
    int num = (int)a;
    while (1) {
        int data = consume_data(&channel1);
        printf("[Worker #%d] Received task: sleeping for %d seconds\n", num, data);
        sleep(data);
        printf("[Worker #%d] Task completed!\n", num);
    }
    return NULL;
}

void* provider(void* a) {
    for (int i = 1; i < 10; i++) {
        printf("[Provider] Sending task: sleeping for %d seconds\n", i);
        provide_data(&channel1, i);
    }
    return NULL;
}

int main() {
    pthread_t provide_thread;
    pthread_t consume_threads[3];
    int result = pthread_create(&provide_thread, NULL, provider, NULL);
    if (result != 0) {
        printf("pthread_create failed for provide thread: %d\n", result);
        return result;
    }
    for (int i = 0; i < 3; i++) {
        int result = pthread_create(&consume_threads[i], NULL, worker, i);
        if (result != 0) {
            printf("pthread_create failed for consume thread #%d: %d\n", i, result);
            return result;
        }
    }
    pthread_join(provide_thread, NULL);
    for (int i = 0; i < 3; i++) {
        pthread_join(consume_threads[i], NULL);
    }
    return 0;
}
